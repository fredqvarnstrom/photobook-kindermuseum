import os
import gc
from kivy.lang import Builder
from kivy.properties import ObjectProperty, ListProperty
from kivy.uix.image import Image, AsyncImage
from kivy.uix.screenmanager import Screen, NoTransition
from widget.page_curl.pagecurl import PageCurlTransition

Builder.load_file(os.path.join(os.path.dirname(__file__), 'book.kv'))


class BookScreen(Screen):

    pages = ListProperty()
    root = ObjectProperty()
    index = 0
    prev_page = None

    def __init__(self, **kwargs):
        super(BookScreen, self).__init__(**kwargs)
        self.index = 0
        self.ids.sm.transition = NoTransition()
        self.switch_page()

    def switch_page(self):
        cur_screen = Screen(name='page_{}'.format(self.index))
        cur_screen.add_widget(AsyncImage(source='data/books/{}'.format(self.pages[self.index]),
                                         nocache=True))
        self.ids.sm.switch_to(cur_screen)
        if self.prev_page:
            self.ids.sm.remove_widget(self.prev_page)
            del self.prev_page
            gc.collect()
        self.prev_page = cur_screen

        self.ids.btn_prev.disabled = True if self.index == 0 else False
        self.ids.btn_prev.opacity = 0 if self.index == 0 else 1
        self.ids.btn_next.disabled = True if self.index == len(self.pages) - 1 else False
        self.ids.btn_next.opacity = 0 if self.index == len(self.pages) - 1 else 1

    def on_btn_next(self):
        if self.index < len(self.pages) - 1 and not self.ids.sm.transition.is_active:
            self.index += 1
            self.ids.sm.transition = PageCurlTransition(duration=1.5)
            self.switch_page()

    def on_btn_prev(self):
        if self.index > 0 and not self.ids.sm.transition.is_active:
            self.index -= 1
            self.ids.sm.transition = PageCurlTransition(duration=1.5, direction='right')
            self.switch_page()

    def _update_me(self):
        self.ids.cur_img.source = self.pages[self.index]
        if self.index < len(self.pages) - 1 and not self.is_transition:
            self.ids.back_img.source = self.page[self.index + 1]

    def on_btn_return(self):
        self.root.go_screen('menu')
