import os
from functools import partial
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.clock import Clock
from utils.xml import get_xml_configuration
from widget.image_button import ImageButton
from kivy.logger import Logger


Builder.load_file(os.path.join(os.path.dirname(__file__), 'menu.kv'))


class MenuScreen(Screen):

    root = ObjectProperty()

    def on_enter(self, *args):
        super(MenuScreen, self).on_enter(*args)
        try:
            bg_image = get_xml_configuration()['root']['settings']['start_screen_background']
        except (KeyError, TypeError, ValueError):
            bg_image = 'background-startscreen.png'
        self.ids.bg.source = 'data/images/{}'.format(bg_image)
        Clock.schedule_once(self.load_book_buttons)

    def load_book_buttons(self, *args):
        books = get_xml_configuration()['root']['books']['book']
        for i, book in enumerate(books):
            img_path = book['menubutton']['@data']
            x_pos = book['menubutton']['@xpos']
            y_pos = book['menubutton']['@ypos']
            btn = ImageButton(source='data/images/{}'.format(img_path), x=x_pos, y=y_pos)
            btn.bind(on_released=partial(self.on_image_button_released, i))
            self.add_widget(btn)

    def on_image_button_released(self, index, *args):
        page_data = get_xml_configuration()['root']['books']['book'][index]['pages']['page']
        pages = [p['@data'] for p in page_data]
        Logger.info('Showing book: {}'.format(pages))
        self.root.show_book(pages)
