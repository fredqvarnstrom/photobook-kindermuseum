import os
import time

import gc
from kivy.app import App
import kivy_config

from kivy import Logger
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import SlideTransition
from screens.book.screen import BookScreen
from screens.menu.screen import MenuScreen
from kivy.lang import Builder
from utils.xml import get_xml_configuration
from kivy.clock import Clock

cur_dir = os.path.dirname(__file__)

Builder.load_file(os.path.join(os.path.dirname(__file__), 'main.kv'))


class PhotoBookScreen(FloatLayout):

    last_active_time = time.time()
    timeout = 30

    screen_names = []
    previous_screen = None
    pages = []

    def __init__(self, **kwargs):
        super(PhotoBookScreen, self).__init__(**kwargs)
        # TODO: Add more screens here.
        self.screen_names = ['menu']
        # Switch to the default screen.
        self.go_screen('menu')

        try:
            self.timeout = int(get_xml_configuration()['root']['settings']['user_timeout'])
        except (KeyError, ValueError, TypeError):
            pass
        Clock.schedule_interval(self.check_activity, 1)

    def go_screen(self, destination):
        """
        Switch the current screen of screen manager
        :param destination:
        :return:
        """
        sm = self.ids.sm
        try:
            if destination == 'menu':
                sm.transition = SlideTransition(direction='right')
                screen = MenuScreen(root=self)
            else:
                sm.transition = SlideTransition(direction='left')
                screen = BookScreen(root=self, pages=self.pages)
            sm.switch_to(screen)
            if self.previous_screen:
                sm.remove_widget(self.previous_screen)
                del self.previous_screen
                gc.collect()
            self.previous_screen = screen
        except Exception as e:
            Logger.error('Failed to move to {} - {}'.format(destination, e))

    def show_book(self, pages):
        self.pages = pages
        self.go_screen('book')

    def on_touch_down(self, touch):
        super(PhotoBookScreen, self).on_touch_down(touch)
        self.last_active_time = time.time()

    def check_activity(self, *args):
        if time.time() - self.last_active_time > self.timeout:
            if self.ids.sm.current != 'menu':
                Logger.warning('PhotoBook: Timeout occurred, moving to the main screen')
                self.go_screen('menu')


class PhotoBookApp(App):

    def build(self):
        self.root = PhotoBookScreen()
        return self.root

    def stop(self, *largs):
        super(PhotoBookApp, self).stop(*largs)
        os.kill(os.getpid(), 9)


if __name__ == '__main__':

    app = PhotoBookApp()
    app.run()
